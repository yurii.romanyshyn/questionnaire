import setuptools


setuptools.setup(
    name="questionnaire-pkg-starnaviio",
    version="0.0.1",
    author="Yurii Romanyshyn",
    author_email="yurii.romanyshyn@starnavi.io",
    description="questionnaire",
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires='>=3.6'
)
