from django.contrib.auth import get_user_model
from django.db import models


UserModel = get_user_model()


class UserProfile(UserModel):
    country = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    linkedin = models.URLField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    user_ptr = models.OneToOneField(
        UserModel,
        parent_link=True,
        primary_key=True,
        on_delete=models.CASCADE,
        related_name="profile"
    )

    class Meta:
        db_table = "user_profiles"


