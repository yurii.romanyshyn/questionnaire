from rest_framework import serializers
from .models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = (
            "id", 
            "username",
            "first_name",
            "last_name",
            "email",
            "country",
            "city",
            "linkedin",
            "facebook"
        )
        read_only_fields = ("id", "username", "email")
