from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from .models import UserProfile


UserModel = get_user_model()


class UserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = (
            "first_name",
            "last_name",
            "country",
            "city",
            "linkedin",
            "facebook"
        )


class RegistrationForm(UserCreationForm):
    class Meta:
        model = UserProfile
        fields = (
            "username",
            "email",
        )

    def clean_username(self):
        username = self.cleaned_data["username"]
        lookup = {"username__exact": username}
        if self.user_exists(lookup) is True:
            raise ValidationError(
                f"user with email {username} alredy exists"
            )
        return username
    
    def clean_email(self):
        email = self.cleaned_data["email"]
        lookup = {"email__exact": email}
        if self.user_exists(lookup) is True:
            raise ValidationError(
                f"user with email {email} alredy exists"
            )
        return email

    def user_exists(self, lookup):
        return bool(
            UserModel.objects
            .filter(**lookup)
            .exists()
        )