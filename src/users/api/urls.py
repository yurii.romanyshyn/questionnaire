from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter
from django.urls import path

from .views import UserViewSet
from .views import PersonalProfileViewSet


users_router = DefaultRouter()
users_router.register("users", UserViewSet)


urlpatterns = [
    path(
        "me/", 
        PersonalProfileViewSet.as_view({
            "get": "retrieve", 
            "put": "update", 
            "patch": "partial_update"
        }),
        name="me"
    ),
]


urlpatterns += users_router.urls
