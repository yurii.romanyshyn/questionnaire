from rest_framework import mixins
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from ..serializers import UserProfileSerializer
from ..models import UserProfile


class PersonalProfileViewSet(
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = UserProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user


class UserViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    queryset = UserProfile.objects.filter(is_active__exact=True).all()
    serializer_class = UserProfileSerializer
    permission_classes = [IsAuthenticated]