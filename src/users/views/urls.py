from django.urls import path
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView

from .views import UserProfileView
from .views import RegistrationView

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("regisration/", RegistrationView.as_view(), name="registration"),
    path("profile/", UserProfileView.as_view(), name="profile")
]