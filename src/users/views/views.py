from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.conf import settings
from django.views import View

from ..forms import UserProfileForm
from ..forms import RegistrationForm


class UserProfileView(LoginRequiredMixin, View): 
    template_name = "users/profile.html"
    form_class = UserProfileForm

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
            "form": self.form_class(instance=request.user.profile)
        })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
        return render(request, self.template_name, context={
            "form": form
        })


class RegistrationView(View):
    template_name = "registration/registration.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("login")

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
            "form": self.form_class()
        })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect(self.success_url)
        else:
            return render(request, self.template_name, context={
                "form": form
            })