from django.conf import settings
from django.urls.base import reverse
from django.http import HttpResponseRedirect


def param_redirect(request, viewname, urlconf=None, **params):
    url = reverse(viewname, urlconf=urlconf)
    if not params:
        return HttpResponseRedirect(url)
    url_params = "&".join([f"{k}={v}" for k, v in params.items()])
    return HttpResponseRedirect(f"{url}?{url_params}")
    