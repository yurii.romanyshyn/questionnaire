from django.contrib import admin

from questionnaire.models import Participant
from questionnaire.models import Blank
from questionnaire.models import Questionnaire
from questionnaire.models import Question
from questionnaire.models import Answer
from questionnaire.models import TextQuestion
from questionnaire.models import SelectQuestion
from questionnaire.models import MultiSelectQuestion
from questionnaire.models import NumberQuestion
from questionnaire.models import TextAnswer
from questionnaire.models import SelectAnswer
from questionnaire.models import MultiSelectAnswer
from questionnaire.models import NumericalAnswer

admin.site.register(Participant)
admin.site.register(Blank)
admin.site.register(Questionnaire)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(TextQuestion)
admin.site.register(SelectQuestion)
admin.site.register(MultiSelectQuestion)
admin.site.register(NumberQuestion)
admin.site.register(TextAnswer)
admin.site.register(SelectAnswer)
admin.site.register(MultiSelectAnswer)
admin.site.register(NumericalAnswer)