from django.urls import path
from .edit_api.urls import urlpatterns as edit_urlpatterns
from .run_api.urls import urlpatterns as run_urlpatterns
from .results_api.urls import urlpatterns as results_urlpatterns

urlpatterns = []
urlpatterns.extend(edit_urlpatterns)
urlpatterns.extend(run_urlpatterns)
urlpatterns.extend(results_urlpatterns)
