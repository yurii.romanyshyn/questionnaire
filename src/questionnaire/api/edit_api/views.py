from django.shortcuts import get_object_or_404
from django.db.models import Prefetch
from django.db.transaction import atomic
from rest_framework import mixins
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from questionnaire.api.permissions import IsOwner
from questionnaire.api.base import ExtendedViewSet
from questionnaire.api.base import ExtendedGenericViewSet
from questionnaire.api.base import ExtendedModelViewSet
from questionnaire.api.pagination import ExtendedLimitOffsetPagination
from questionnaire.serializers import get_question_serializer
from questionnaire.serializers import BlankSerializer
from questionnaire.serializers import QuestionReprSerializer
from questionnaire.models import Blank
from questionnaire.models import Question
from questionnaire.models.constants import QuestionType


class BlankEditViewSet(ExtendedModelViewSet):
    permission_classes = [IsAuthenticated, IsOwner]
    serializer_class = BlankSerializer
    lookup_field = "slug"
    lookup_url_kwarg = "blank_slug"
    
    def get_queryset(self):
        user = self.request.user
        if self.action in ("retrieve", "list"):
            return (
                Blank.objects
                .with_stat()
                .filter(owner__pk=user.pk)
                .all()
            )
        else:
            return (
                Blank.objects
                .filter(owner__pk=user.pk)
                .all()
            )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        serializer.save()

    @action(methods=["DELETE"], detail=True)
    def clean(self, request, *args, **kwargs):
        blank = self.get_object()
        with atomic():
            blank.questions.all().delete()
            blank.is_active = False
            blank.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class QuestionTypeSerializer(serializers.Serializer):
    type = serializers.ChoiceField(
        required=True,
        choices=[(t.value, t.name) for t in QuestionType]
    )


class ChangeQuestionIndexSerializer(serializers.Serializer):
    index = serializers.IntegerField(min_value=0)


class QuestionEditViewSet(ExtendedViewSet):
    permission_classes = [IsAuthenticated, IsOwner]
    paginator_class = ExtendedLimitOffsetPagination
    
    blank_lookup = "slug__exact"
    blank_url_kwarg = "blank_slug"
    question_lookup = "pk"
    question_url_kwarg = "question_pk"

    def create(self, request, *args, **kwargs):
        blank = self.get_blank()
        QuestionSerializer = self.get_question_serializer_class()
        question_serializer = QuestionSerializer(
            data=request.data,
            context={"request": request}
        )
        question_serializer.is_valid(raise_exception=True)
        questions_count = blank.questions.count()
        last_index = questions_count + 1
        question_serializer.save(blank=blank, index=last_index)
        return Response(
            question_serializer.data,
            status=status.HTTP_201_CREATED
        )

    def retrieve(self, request, *args, **kwargs):
        question = self.get_question()
        return Response(QuestionReprSerializer(instance=question).data)

    def list(self, request, *args, **kwargs):
        blank = self.get_blank()
        questions_queryset = blank.questions.order_by("index").all()
        paginator = self.paginator_class()
        page_data = paginator.paginate_queryset(questions_queryset, request, self)
        serializer = QuestionReprSerializer(page_data, many=True)
        return paginator.get_paginated_response(serializer.data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.get("partial", False)
        
        question = self.get_question()
        specific_question = getattr(question, question.type)
        QuestionSerializer = get_question_serializer(question.type)
        
        serializer = QuestionSerializer(
            data=request.data,
            instance=specific_question,
            partial=partial,
            context={"request": request}
        )
        serializer.is_valid(raise_exception=True)

        with atomic():
            serializer.save()
            if "is_active" in request.data:
                blank = self.get_blank()
                if blank.questions.filter(is_active=True).count() == 0:
                    blank.is_active = False
                    blank.save()

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        blank = self.get_blank()
        question = self.get_question()

        with atomic():
            question.delete()
            
            if blank.questions.filter(is_active=True).count() == 0:
                blank.is_active = False
                blank.save()
                return Response(status=status.HTTP_204_NO_CONTENT)            

            questions_with_gt_index = list(
                blank.questions
                .filter(index__gt=question.index)
                .all()
            )
            
            to_update = []
            for question in questions_with_gt_index:
                question.index -= 1
                to_update.append(question)
            
            Question.objects.bulk_update(to_update, ["index"])

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=["POST"], detail=True)
    def change_question_index(self, request, *args, **kwargs):
        serializer = ChangeQuestionIndexSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_index = serializer.validated_data["index"]

        blank = self.get_blank()
        current_question = self.get_question()
        blank_questions = list(
            blank.questions
            .exclude(pk=current_question.pk)
            .all()
        )
        questions_count = len(blank_questions) + 1

        if new_index > questions_count:
            current_question.index = questions_count
        else:
            current_question.index = new_index

        to_update = [current_question,]
        
        for question in blank_questions:
            if question.index <= current_question.index:
                question.index -= 1
                to_update.append(question)
        
        Question.objects.bulk_update(to_update, ["index"])
        return Response(status=status.HTTP_200_OK)

    def get_blank(self):
        if getattr(self, "blank", None) is None:
            lookup_value = self.kwargs[self.blank_url_kwarg]
            lookup = {self.blank_lookup: lookup_value}
            queryset = (
                Blank.objects
                .filter(owner__pk=self.request.user.pk)
                .all()
            )
            self.blank = get_object_or_404(queryset, **lookup)
            self.check_object_permissions(self.request, self.blank)
        return self.blank
    
    def get_question(self):
        if getattr(self, "question", None) is None:
            question_lookup_value = self.kwargs[self.question_url_kwarg]
            blank = self.get_blank()
            self.question = get_object_or_404(
                blank.questions.all(),
                **{self.question_lookup: question_lookup_value}
            )
        
        return self.question

    def get_question_serializer_class(self):
        question_type_serializer = QuestionTypeSerializer(data=self.request.data)
        question_type_serializer.is_valid(raise_exception=True)
        QuestionSerializer = get_question_serializer(self.request.data["type"])
        return QuestionSerializer