from django.urls import path
from .views import BlankEditViewSet
from .views import QuestionEditViewSet


urlpatterns = [
    
    # Blank urls
    path(
        "blanks/", 
        BlankEditViewSet.as_view({
            "get": "list", 
            "post": "create"
        }), 
        name="api-blank-list"
    ),
    path(
        "blanks/<slug:blank_slug>/", 
        BlankEditViewSet.as_view({
            "get": "retrieve",
            "put": "update",
            "patch": "partial_update",
            "delete": "destroy"
        }),
        name="api-blank-detail"
    ),
    path(
        "blanks/<slug:blank_slug>/clean/",
        BlankEditViewSet.as_view({
            "delete": "clean"
        }),
        name="api-blank-clean"
    ),
    
    # Question url
    path(
        "blanks/<slug:blank_slug>/questions/",
        QuestionEditViewSet.as_view({
            "get": "list",
            "post": "create"
        }),
        name="api-question-list"
    ),
    path(
        "blanks/<slug:blank_slug>/questions/<int:question_pk>/",
        QuestionEditViewSet.as_view({
            "get": "retrieve",
            "put": "update",
            "patch": "partial_update",
            "delete": "destroy"
        }),
        name="api-question-detail"
    ),
    path(
        "blanks/<slug:blank_slug>/questions/<int:question_pk>/change-index/",
        QuestionEditViewSet.as_view({"post": "change_question_index"}),
        name="ap-question-change-index"
    )
]