from rest_framework import serializers
from questionnaire.models import Participant


class BlankOwnerSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255)
    email = serializers.EmailField()
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)


class BlankReprSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255)
    is_public = serializers.BooleanField()
    with_email_verification = serializers.BooleanField()
    description = serializers.CharField()
    questions_count = serializers.SerializerMethodField()
    owner = BlankOwnerSerializer(many=False)

    def get_questions_count(self, obj):
        return obj.questions.filter(is_active=True).count()


class ParticipantSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    email = serializers.EmailField()

    def save(self):
        try:
            participant = Participant.objects.get(
                email__exact=self.validated_data["email"]
            )
            if participant.user is None and self.context.get("user") is not None:
                participant.user = self.context["user"]
                participant.save()
            return participant
        except Participant.DoesNotExist:
            return Participant.objects.create(
                first_name=self.validated_data["first_name"],
                last_name=self.validated_data["last_name"],
                email=self.validated_data["email"],
                user=self.context.get("user"),
            )
