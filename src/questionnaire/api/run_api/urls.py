from django.urls import path
from .views import BlankPublicViewSet
from .views import QuestionnaireViewSet


urlpatterns = [
    
    path(
        "blanks/<slug:blank_slug>/show/",
        BlankPublicViewSet.as_view({"get": "show"}),
        name="api-blank-show"
    ),
    
    path(
        "blanks/<slug:blank_slug>/become-participant/",
        BlankPublicViewSet.as_view({"post": "become_participant"}),
        name="api-become-participant"
    ),
    
    path(
        "questionnaire/<uuid:questionnaire_uuid>/start/",
        QuestionnaireViewSet.as_view({"post": "start"}),
        name="api-start-questionnaire"
    ),
    
    path(
        "questionnaire/<uuid:questionnaire_uuid>/question/<int:question_pk>/answer/",
        QuestionnaireViewSet.as_view({"post": "answer"}),
        name="api-answer-questionnaire"
    ),
    
    path(
        "questionnaire/<uuid:questionnaire_uuid>/cancel/",
        QuestionnaireViewSet.as_view({"post": "cancel"}),
        name="api-cancel-questionnaire"
    ),
    
    path(
        "questionnaire/<uuid:questionnaire_uuid>/done/",
        QuestionnaireViewSet.as_view({"post": "done"}),
        name="api-done-questionnaire"
    )

]