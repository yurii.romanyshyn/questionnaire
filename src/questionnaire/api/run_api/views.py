from datetime import datetime
from django.shortcuts import get_object_or_404
from django.db.models import Prefetch
from django.db.transaction import atomic
from django.urls import reverse
from django.core.exceptions import ValidationError as CoreValidationError
from rest_framework import serializers
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import action

from .serializers import BlankReprSerializer
from .serializers import ParticipantSerializer
from questionnaire.api.permissions import CanBecomeParticipant
from questionnaire.api.permissions import CanRunQuestionnaire
from questionnaire.serializers import get_answer_serializer
from questionnaire.serializers import QuestionReprSerializer
from questionnaire.models import Blank
from questionnaire.models import Questionnaire
from questionnaire.models import Question
from questionnaire.models import Answer
from questionnaire.models import Participant
from questionnaire.models import get_question_answer_model
from questionnaire.models.constants import QuestionType
from questionnaire.utils import parse_user_agent_string 


class BlankPublicViewSet(ViewSet):
    permission_classes = [CanBecomeParticipant]
    blank_lookup = "slug__exact"
    blank_url_kwarg = "blank_slug"

    @action(methods=["GET"], detail=True)
    def show(self, *args, **kwargs):
        blank = self.get_blank(select_related=["owner"])
        serializer = BlankReprSerializer(instance=blank)
        return Response(serializer.data)

    @action(methods=["POST"], detail=True)
    def become_participant(self, request, *args, **kwargs):
        blank = self.get_blank()
        participant = self.get_participant()
        user_agent = self.get_user_agent()
        questionnaire = Questionnaire.objects.create(
            blank=blank,
            participant=participant,
            participant_device=user_agent["device"],
            participant_operation_system=user_agent["device"],
            participant_browser=user_agent["browser"]
        )
        request.session[str(questionnaire.uuid)] = questionnaire.access_code
        questionnaire_url = reverse(
            "api-start-questionnaire", 
            kwargs={"questionnaire_uuid": questionnaire.uuid}
        )
        return Response({"questionnaire": questionnaire_url})

    def get_blank(self, prefetch_related=None, select_related=None):
        if getattr(self, "blank", None) is None:
            blank_lookup_value = self.kwargs[self.blank_url_kwarg]
            queryset = Blank.objects.filter(is_active=True).all()

            if prefetch_related:
                queryset = queryset.prefetch_related(*prefetch_related)
            
            if select_related:
                queryset = queryset.select_related(*select_related)
            
            lookup = {self.blank_lookup: blank_lookup_value}
            self.blank = get_object_or_404(queryset, **lookup)
            self.check_object_permissions(self.request, self.blank)
        
        return self.blank

    def get_participant(self):
        if getattr(self, "participant", None) is None:
            if self.request.user.is_authenticated:
                self.participant = self.get_user_participant_profile()
            else:
                self.participant = self.get_participant_profile()
        return self.participant

    def get_user_participant_profile(self):
        user = self.request.user
        serializer = ParticipantSerializer(
            data={
                "first_name": user.first_name,
                "last_name": user.last_name,
                "email": user.email,
            },
            context={
                "user": user
            }
        )
        serializer.is_valid()
        return serializer.save()
        
    def get_participant_profile(self):
        participant_serializer = ParticipantSerializer(data=self.request.data)
        participant_serializer.is_valid(raise_exception=True)
        return participant_serializer.save()

    def get_user_agent(self):
        user_agent_string = self.request.META["HTTP_USER_AGENT"]
        user_agent = parse_user_agent_string(user_agent_string)
        return user_agent


class QuestionnaireViewSet(ViewSet):
    permission_classes = [CanRunQuestionnaire]
    questionnaire_lookup = "uuid__exact"
    questionnaire_url_kwarg = "questionnaire_uuid"
    question_url_kwarg = "question_pk"
    question_lookup = "pk"

    @action(methods=["POST"], detail=True)
    def start(self, request, *args, **kwargs):
        questionnaire = self.get_questionnaire(select_related=[
            "blank",
            "participant"
        ])

        if questionnaire.started_at is not None:
            return Response(
                {"detail": "Questionnaire is alredy started"},
                status=status.HTTP_410_GONE
            )
        
        questionnaire.started_at = datetime.now()
        questionnaire.save()
        
        questions = (
            questionnaire.blank.questions
            .filter(is_active=True)
            .order_by("index")
            .all()
        )
        
        question_serializer = QuestionReprSerializer(instance=questions, many=True)
        
        return Response(
            {"questions": question_serializer.data},
            status=status.HTTP_200_OK
        )
    
    @action(methods=["POST"], detail=True)
    def answer(self, request, *args, **kwargs):
        question = self.get_question()
        questionnaire = self.get_questionnaire()

        if not questionnaire.is_in_progress:
            return Response(
                {"detail": "You did not start questionnaire"},
                status=status.HTTP_400_BAD_REQUEST
            )
        
        if question.is_answered(questionnaire):
            return Response(
                {"detail": "You already answered on this question"},
                status=status.HTTP_410_GONE
            )
        
        AnswerSerializer = get_answer_serializer(question.type)
        serializer = AnswerSerializer(
            data=request.data,
            context={"question": getattr(question, question.type)}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save(
            question=question,
            questionnaire=questionnaire
        )
        return Response(serializer.data)

    @action(methods=["POST"], detail=True)
    def done(self, request, *args, **kwargs):
        
        questionnaire = self.get_questionnaire(
            select_related=["blank", "participant"],
            prefetch_related=[
                Prefetch(
                    "blank__questions",
                    queryset=(
                        Question.objects
                        .select_related(*[qt.value for qt in QuestionType])
                        .filter(is_active=True)
                        .all()
                    )
                ),
                Prefetch(
                    "answers",
                    queryset=Answer.objects.all()
                )
            ]
        )

        if not questionnaire.is_in_progress:
            return Response(
                {"detail": "You did not start questionnaire"},
                status=status.HTTP_400_BAD_REQUEST
            )

        questions_pk_set = set()
        mandatory_questions_pk_set = set()

        for question in questionnaire.blank.questions.all():
            questions_pk_set.add(question.pk)
            if question.is_mandatory:
                mandatory_questions_pk_set.add(question.pk)

        answered_questions_pk_set = {
            answer.question.pk
            for answer in questionnaire.answers.all()
        }

        unanswered_questions_pk_set = questions_pk_set.difference(
            answered_questions_pk_set
        )

        if len(unanswered_questions_pk_set) == 0:
            questionnaire.finished_at = datetime.now()
            questionnaire.save()
            return Response(status=status.HTTP_200_OK)

        unanswered_mandatory_questions_pk_set = mandatory_questions_pk_set.intersection(
            unanswered_questions_pk_set
        )

        if len(unanswered_mandatory_questions_pk_set) > 0:
            return Response(
                {"detail": "You did not answered on mandatory questions"},
                status=status.HTTP_400_BAD_REQUEST
            )

        unanswered_questions = [
            getattr(question, question.type)
            for question in questionnaire.blank.questions.all()
            if question.pk in unanswered_questions_pk_set
        ]

        try:
            
            with atomic():
                for question in unanswered_questions:
                    AnswerModel = get_question_answer_model(question.type)
                    AnswerModel(
                        question=question,
                        questionnaire=questionnaire,
                        value=question.default
                    ).store()
                
                questionnaire.finished_at = datetime.now()
                questionnaire.save()

                request.session[str(questionnaire.uuid)] = None
        
        except CoreValidationError as error:
            raise ValidationError(error.message)

        return Response(status=status.HTTP_200_OK)


    @action(methods=["POST"], detail=True)
    def cancel(self, request, *args, **kwargs):
        questionnaire = self.get_questionnaire()
        questionnaire.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_questionnaire(self, prefetch_related=None, select_related=None):
        if getattr(self, "questionnaire", None) is None:
            lookup_value = self.kwargs[self.questionnaire_url_kwarg]
            lookup = {self.questionnaire_lookup: lookup_value}
            queryset = (
                Questionnaire.objects
                .filter(finished_at__isnull=True)
                .all()
            )

            if prefetch_related:
                queryset = queryset.prefetch_related(*prefetch_related)

            if select_related:
                queryset = queryset.select_related(*select_related)
            
            self.questionnaire = get_object_or_404(queryset, **lookup)
            self.check_object_permissions(self.request, self.questionnaire)
        
        return self.questionnaire

    def get_question(self):
        if getattr(self, "question", None) is None:
            questionnaire = self.get_questionnaire(select_related=["blank"])
            
            question_lookup_value = self.kwargs[self.question_url_kwarg]
            question_lookup = {self.question_lookup: question_lookup_value}

            self.question = get_object_or_404(
                questionnaire.blank.questions.filter(is_active=True).all(),
                **question_lookup
            )
        
        return self.question