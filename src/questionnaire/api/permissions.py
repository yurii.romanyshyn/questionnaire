from rest_framework.permissions import SAFE_METHODS
from rest_framework.permissions import BasePermission


class CanBecomeParticipant(BasePermission):
    def has_object_permission(self, request, view, obj):
        return (
            obj.is_public
            or request.user.is_authenticated
        )


class CanRunQuestionnaire(BasePermission):
    def has_object_permission(self, request, view, obj):
        return (
            (
                request.user.is_authenticated
                and obj.participant.user.pk == request.user.pk
            )
            or request.session.get(str(obj.uuid)) == obj.access_code
        )


class IsOwner(BasePermission):

    def __init__(self, *args, **kwargs):
        self.field_name = kwargs.pop("field_name", "owner")
        super().__init__(*args, **kwargs)

    def has_object_permission(self, request, view, obj):
        if callable(self.field_name):
            object_owner = self.field_name(obj)
        else:
            object_owner = getattr(obj, self.field_name)
        return request.user == object_owner


class IsOwnerOrReadOnly(IsOwner):

    def has_object_permission(self, request, view, obj):
        if request.method not in SAFE_METHODS:
            return super().has_object_permission(request, view, obj)
        return True