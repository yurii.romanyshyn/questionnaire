from django.urls import path
from .views import QuestionnaireViewSet


urlpatterns = [
    path(
        "blanks/<slug:blank_slug>/results/",
        QuestionnaireViewSet.as_view({"get": "list"}),
        name="api-questionnaire-results"
    ),
    path(
        "blanks/<slug:blank_slug>/results/<int:questionnaire_pk>/",
        QuestionnaireViewSet.as_view({
            "get": "retrieve",
            "delete": "destroy"
        }),
        name="api-questionnaire-detail"
    ),
]