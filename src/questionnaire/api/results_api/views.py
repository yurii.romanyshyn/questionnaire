from django.shortcuts import get_object_or_404
from django.db.models import Prefetch
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from questionnaire.api.base import ExtendedGenericViewSet
from questionnaire.api.permissions import IsOwner
from questionnaire.api.pagination import ExtendedLimitOffsetPagination
from questionnaire.models import Blank
from questionnaire.models import Questionnaire
from questionnaire.models import Answer
from questionnaire.models.constants import QUESTIONS_ANSWERS

from .serializers import QuestionnaireSerializer
from .serializers import DetailQuestionnaireSerializer


class QuestionnaireViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated, IsOwner]
    paginator_class = ExtendedLimitOffsetPagination
    blank_lookup = "slug__exact"
    blank_url_kwargs = "blank_slug"
    questionnaire_lookup = "pk"
    questionnaire_url_kwargs = "questionnaire_pk"

    def retrieve(self, request, *args, **kwargs):
        answers_to_prefetch = list(QUESTIONS_ANSWERS.values())
        answers_prefetch_queryset = (
            Answer.objects
            .select_related("question")
            .prefetch_related(*answers_to_prefetch)
            .order_by("made_at")
            .all()
        )
        questionnaire = self.get_questionnaire_object(
            select_related=["participant"],
            prefetch_related=[
                Prefetch(
                    "answers", 
                    queryset=answers_prefetch_queryset
                ),
            ]
        )
        serializer = DetailQuestionnaireSerializer(instance=questionnaire)
        return Response(serializer.data)
        
    def list(self, request, *args, **kwargs):
        paginator = self.paginator_class()
        page_data = paginator.paginate_queryset(
            self.get_questionnaires(select_related=["participant"]), 
            request, 
            self
        )
        serializer = QuestionnaireSerializer(page_data, many=True)
        return paginator.get_paginated_response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        questionnaire = self.get_questionnaire_object()
        questionnaire.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_blank(self):
        if getattr(self, "blank", None) is None:
            lookup_value = self.kwargs[self.blank_url_kwargs]
            lookup = {self.blank_lookup: lookup_value}
            self.blank = get_object_or_404(
                Blank.objects.all(),
                **lookup
            )
            self.check_object_permissions(self.request, self.blank)
        return self.blank

    def get_questionnaires(self, select_related=None, prefetch_related=None):
        blank = self.get_blank()
        queryset = (
            blank.questionnaires
            .filter(finished_at__isnull=False)
            .order_by("started_at")
            .all()
        )
            
        if select_related:
            queryset = queryset.select_related(*select_related)
        
        if prefetch_related:
            queryset = queryset.prefetch_related(*prefetch_related)
        
        return queryset
        
    def get_questionnaire_object(self, select_related=None, prefetch_related=None):
        if getattr(self, "questionnaire_object", None) is None:
            blank = self.get_blank()
            lookup_value = self.kwargs[self.questionnaire_url_kwargs]
            lookup = {self.questionnaire_lookup: lookup_value}
            queryset = (
                blank.questionnaires
                .filter(finished_at__isnull=False)
                .order_by("started_at")
            )
            
            if select_related:
                queryset = queryset.select_related(*select_related)
            
            if prefetch_related:
                queryset = queryset.prefetch_related(*prefetch_related)

            self.questionnaire_object = get_object_or_404(queryset, **lookup)
        
        return self.questionnaire_object