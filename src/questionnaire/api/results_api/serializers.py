from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import serializers

from questionnaire.serializers import AnswerReprSerializer


class ParticipantSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    email = serializers.EmailField()
    user = serializers.PrimaryKeyRelatedField(
        required=False,
        queryset=User.objects.all()
    )


class QuestionnaireSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    uuid = serializers.UUIDField()
    participant = ParticipantSerializer()
    participant_device = serializers.CharField()
    participant_operation_system = serializers.CharField()
    participant_browser = serializers.CharField()
    created_at = serializers.DateTimeField()
    started_at = serializers.DateTimeField()
    finished_at = serializers.DateTimeField()


class DetailQuestionnaireSerializer(QuestionnaireSerializer):
    answers = AnswerReprSerializer(many=True, resolve_questions=True)