from rest_framework import status
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.response import Response


class GetPermissionsMixin:
    def get_permissions(self):
        return [
            (
                permission() 
                if isinstance(permission, type)
                else permission
            )
            for permission in getattr(self, "permission_classes", [])
        ]


class ExtendedViewSet(
    GetPermissionsMixin,
    viewsets.ViewSet
):
    pass


class ExtendedGenericViewSet(
    GetPermissionsMixin, 
    viewsets.GenericViewSet
):
    pass


class ExtendedModelViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    ExtendedGenericViewSet
):
    pass