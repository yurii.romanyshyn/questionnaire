from django import forms
from django.core.exceptions import ValidationError
from questionnaire.models import TextQuestion
from questionnaire.models import SelectQuestion
from questionnaire.models import MultiSelectQuestion
from questionnaire.models import NumberQuestion
from questionnaire.models.constants import QuestionType


general_fields = (
    "type",
    "text",
    "post_text",
    "is_active",
    "is_mandatory",
    "default"
)


class TextQuestionForm(forms.ModelForm):
    type = forms.CharField(
        initial=QuestionType.TEXT.value,
        widget=forms.HiddenInput()
    )

    class Meta:
        model = TextQuestion
        fields = general_fields + ("max_length",)


class SelectQuestionForm(forms.ModelForm):
    type = forms.CharField(
        initial=QuestionType.SELECT.value,
        widget=forms.HiddenInput()
    )

    class Meta:
        model = SelectQuestion
        fields = general_fields + ("choices",)


class MultiSelectQuestionForm(forms.ModelForm):
    type = forms.CharField(
        initial=QuestionType.MULTI_SELECT.value,
        widget=forms.HiddenInput()
    )
    
    class Meta:
        model = MultiSelectQuestion
        fields = general_fields + ("choices",)


class NumberQuestionForm(forms.ModelForm):
    type = forms.CharField(
        initial=QuestionType.NUMBER.value,
        widget=forms.HiddenInput()
    )
    
    class Meta:
        model = NumberQuestion
        fields = general_fields + ("min_value", "max_value")


def get_question_form(question_type):
    forms = {
        QuestionType.TEXT.value: TextQuestionForm,
        QuestionType.SELECT.value: SelectQuestionForm,
        QuestionType.MULTI_SELECT.value: MultiSelectQuestionForm,
        QuestionType.NUMBER.value: NumberQuestionForm,
    }
    try:
        return forms[question_type]
    except KeyError:
        raise ValueError(f"Unknown question type {question_type}")