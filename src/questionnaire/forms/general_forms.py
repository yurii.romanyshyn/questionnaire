from django import forms
from questionnaire.models import Blank


class CreateBlankForm(forms.ModelForm):
    class Meta:
        model = Blank
        fields = (
            "title",
            "description",
            "is_public",
            "with_email_verification"
        )
