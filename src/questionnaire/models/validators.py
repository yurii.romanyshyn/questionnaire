from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from .constants import TEXT_QUESTION_DEFAUT_LENGTH


@deconstructible
class ChoicesValidator:
    code = "invalid_choice_str"

    def __init__(self, choice_max_length):
        self.max_length = choice_max_length

    def __call__(self, value):
        if not isinstance(value, (list, tuple)):
            raise ValidationError(
                f"Excpecting list, but received - {type(value)}",
                code=self.code
            )

        if len(value) == 0:
            raise ValidationError(
                "choices list cannot be empty",
                code=self.code
            )
            
        if any(not isinstance(choice, str) for choice in value):
            raise ValidationError(
                f"Excpecting list of strings", 
                code=self.code
            )

        if any(len(choice) > self.max_length for choice in value):
            raise ValidationError(
                f"Choice max_length is {self.max_length}", 
                code=self.code
            )

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.max_length == other.max_length


class MultifunctionalValidator:
    requires_context = True
    
    def __init__(self, *args, **kwargs):
        self.data = None
        self.serializer = None
        self.model = None

    def __call__(self, data, serializer=None, model=None):
        if serializer:
            self.data = data
            self.serializer = serializer
            self.validate_serializer()
        else:
            self.data = data
            self.model = model
            self.validate_model()

    def get_instance_attr(self, name):
        instance = self.serializer.instance
        return (
            getattr(instance, name, None) 
            if instance is not None 
            else None
        )

    def get_data_or_instance_attr(self, name):
        if name in self.data:
            return self.data[name]
        return self.get_instance_attr(name)
    
    def validate_model(self):
        raise NotImplementedError

    def validate_serializer(self):
        raise NotImplementedError

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return True


class BlankValidator(MultifunctionalValidator):
    message = "You cannot make questionnaire blank active without any active question"

    def validate_model(self):
        if self.model.pk is None:
            return
        if self.model.is_active:
            questions_exists = self.model.questions.filter(is_active=True).exists()
            if not questions_exists:
                raise ValidationError(self.message)

    def validate_serializer(self):
        if self.serializer.instance is not None:
            questions_exists = (
                self.serializer.instance.questions
                .filter(is_active=True)
                .exists()
            )
            if self.data.get("is_active", False) and not questions_exists:
                raise ValidationError(self.message)
        else:
            if self.data.get("is_active", False):
                raise ValidationError(self.message)


class TextQuestionValidator(MultifunctionalValidator):
    message = "len of 'default' is greater that 'max_length'"

    def validate(self, default, max_length):
        if default is None:
            return
        if len(default) > max_length:
            raise ValidationError(self.message)

    def validate_model(self):
        default = self.data.get("default")
        max_length = self.data.get("max_length", TEXT_QUESTION_DEFAUT_LENGTH)
        self.validate(default, max_length)

    def validate_serializer(self):
        default = self.get_data_or_instance_attr("default")
        max_length = (
            self.get_data_or_instance_attr("max_length") 
            or TEXT_QUESTION_DEFAUT_LENGTH
        )
        self.validate(default, max_length)


class SelectQuestionValidator(MultifunctionalValidator):
    message = "'default' value is not present in 'choices'"

    def validate(self, default, choices):
        if default is None:
            return
        if default not in choices:
            raise ValidationError(self.message)

    def validate_model(self):
        default = self.data.get("default")
        choices = self.data["choices"]
        self.validate(default, choices)

    def validate_serializer(self):
        default = self.get_data_or_instance_attr("default")
        choices = self.get_data_or_instance_attr("choices")
        self.validate(default, choices)


class MultiSelectQuestionValidator(MultifunctionalValidator):
    message = "'default' values is not present in 'choices'"

    def validate(self, default, choices):
        if default is None:
            return
        if not all(choice in choices for choice in default):
            raise ValidationError(self.message)

    def validate_model(self):
        default = self.data.get("default")
        choices = self.data["choices"]
        self.validate(default, choices)

    def validate_serializer(self):
        default = self.get_data_or_instance_attr("default")
        choices = self.get_data_or_instance_attr("choices")
        self.validate(default, choices)
    

class NumberQuestionValidator(MultifunctionalValidator):

    def validate(self, min_value, max_value, default):
        if max_value <= min_value:
            raise ValidationError("'max_value' cannot be less-equal that min_value")
        
        if default is None:
            return
        
        if not (min_value <= default <= max_value):
            raise ValidationError(
                f"'default' must be in range from {min_value} to {max_value}"
            )

    def validate_model(self):
        min_value = self.data["min_value"]
        max_value = self.data["max_value"]
        default = self.data.get("default")
        self.validate(min_value, max_value, default)

    def validate_serializer(self):
        min_value = self.get_data_or_instance_attr("min_value")
        max_value = self.get_data_or_instance_attr("max_value")
        default = self.get_data_or_instance_attr("default")
        self.validate(min_value, max_value, default)


class TextAnswerValidator(MultifunctionalValidator):
    message = "Exceeded max_length"

    def validate(self, value, max_length):
        if value is None:
            return
        if len(value) > max_length:
            raise ValidationError(self.message)

    def validate_serializer(self):
        question = self.serializer.context["question"]
        value = self.get_data_or_instance_attr("value")
        max_length = question.max_length
        print(self.serializer.context)
        self.validate(value, max_length)

    def validate_model(self):
        value = self.model.value
        max_length = self.model.question.max_length
        self.validate(value, max_length)


class SelectAnswerValidator(MultifunctionalValidator):
    message = "'value' must be part of 'select_question.choices' list"

    def validate(self, value, choices):
        if value is None:
            return
        if value not in choices:
            raise ValidationError(self.message)

    def validate_serializer(self):
        question = self.serializer.context["question"]
        value = self.get_data_or_instance_attr("value")
        choices = question.choices
        self.validate(value, choices)

    def validate_model(self):
        value = self.model.value
        choices = self.model.question.select_question.choices
        self.validate(value, choices)


class MultiSelectAnswerValidator(MultifunctionalValidator):
    message = "'value' must be part of 'multi_select.choices' list"

    def validate(self, value, choices):
        if value is None:
            return
        if any(v not in choices for v in value):
            raise ValidationError(self.message)

    def validate_serializer(self):
        question = self.serializer.context["question"]
        value = self.get_data_or_instance_attr("value")
        choices = question.choices
        self.validate(value, choices)

    def validate_model(self):
        value = self.model.value
        choices = self.model.question.multi_select_question.choices
        self.validate(value, choices)


class NumericalAnswerValidator(MultifunctionalValidator):
    message = "'value' must be in range from {0} to {1}"

    def validate(self, value, min_value, max_value):
        if value is None:
            return
        if not (min_value <= value <= max_value):
            raise ValidationError(self.message.format(min_value, max_value))

    def validate_serializer(self):
        question = self.serializer.context["question"]
        value = self.get_data_or_instance_attr("value")
        min_value = question.min_value
        max_value = question.max_value
        self.validate(value, min_value, max_value)

    def validate_model(self):
        min_value = self.model.question.number_picker.min_value
        max_value = self.model.question.number_picker.max_value
        value = self.model.value
        self.validate(value, min_value, max_value)