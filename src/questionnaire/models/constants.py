from enum import Enum

TEXT_QUESTION_DEFAUT_LENGTH = 2000
TEXT_QUESTION_MAX_LENGTH = 5000
CHOICE_MAX_LENGTH = 255

class QuestionType(str, Enum):
    TEXT = "text_question"
    SELECT = "select_question"
    MULTI_SELECT = "multi_select_question"
    NUMBER = "number_question"

    @classmethod
    def from_value(cls, value):
        for entry in cls:
            if entry.value == value:
                return entry
        raise ValueError("Unknown value")


QUESTIONS_ANSWERS = {
    QuestionType.TEXT.value: "text_answer",
    QuestionType.SELECT.value: "select_answer",
    QuestionType.MULTI_SELECT.value: "multi_select_answer",
    QuestionType.NUMBER.value: "numerical_answer"
}