from django.db import models
from django.core.exceptions import ValidationError

from .main_models import Answer
from .mixins import CheckQuestionTypeMixin
from .validators import ChoicesValidator
from .validators import TextAnswerValidator
from .validators import SelectAnswerValidator
from .validators import MultiSelectAnswerValidator
from .validators import NumericalAnswerValidator
from .constants import QuestionType
from .constants import TEXT_QUESTION_MAX_LENGTH
from .constants import CHOICE_MAX_LENGTH
from .constants import QUESTIONS_ANSWERS


class TextAnswer(CheckQuestionTypeMixin, Answer):
    value = models.TextField(
        max_length=TEXT_QUESTION_MAX_LENGTH,
        null=True,
        blank=True
    )
    answer_ptr = models.OneToOneField(
        Answer,
        on_delete=models.CASCADE,
        primary_key=True,
        parent_link=True,
        related_name=QUESTIONS_ANSWERS[QuestionType.TEXT.value]
    )

    QUESTION_TYPE = QuestionType.TEXT
    validators = [TextAnswerValidator()]

    class Meta:
        db_table = "text_answers"


class SelectAnswer(CheckQuestionTypeMixin, Answer):    
    value = models.CharField(
        max_length=CHOICE_MAX_LENGTH,
        null=True,
        blank=True
    )
    answer_ptr = models.OneToOneField(
        Answer,
        on_delete=models.CASCADE,
        primary_key=True,
        parent_link=True,
        related_name=QUESTIONS_ANSWERS[QuestionType.SELECT.value]
    )

    QUESTION_TYPE = QuestionType.SELECT
    validators = [SelectAnswerValidator()]

    class Meta:
        db_table = "select_answers"


class MultiSelectAnswer(CheckQuestionTypeMixin, Answer):
    value = models.JSONField(
        validators=[ChoicesValidator(CHOICE_MAX_LENGTH)],
        null=True, 
        blank=True
    )
    answer_ptr = models.OneToOneField(
        Answer,
        on_delete=models.CASCADE,
        primary_key=True,
        parent_link=True,
        related_name=QUESTIONS_ANSWERS[QuestionType.MULTI_SELECT.value]
    )

    QUESTION_TYPE = QuestionType.MULTI_SELECT
    validators = [MultiSelectAnswerValidator()]

    class Meta:
        db_table = "multi_select_answers"


class NumericalAnswer(CheckQuestionTypeMixin, Answer):
    value = models.IntegerField(
        null=True,
        blank=True
    )
    answer_ptr = models.OneToOneField(
        Answer,
        on_delete=models.CASCADE,
        primary_key=True,
        parent_link=True,
        related_name=QUESTIONS_ANSWERS[QuestionType.NUMBER.value]
    )

    QUESTION_TYPE = QuestionType.NUMBER
    validators = [NumericalAnswerValidator()]

    class Meta:
        db_table = "numerical_answers"


def get_question_answer_model(question_type_value):
    models = {
        QuestionType.TEXT.value: TextAnswer,
        QuestionType.SELECT.value: SelectAnswer,
        QuestionType.MULTI_SELECT.value: MultiSelectAnswer,
        QuestionType.NUMBER.value: NumericalAnswer
    }
    return models[question_type_value]