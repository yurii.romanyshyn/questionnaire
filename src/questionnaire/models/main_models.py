from functools import partial
from uuid import uuid4
from django.contrib.auth.models import User
from django.db import models

from .base import BaseModel
from .validators import BlankValidator
from .constants import QuestionType
from ..utils import slugify
from ..utils import generate_random_string


class BlankManager(models.Manager):
    
    def with_stat(self):
        return super().get_queryset().annotate(
            questions_count=models.Count("questions"),
            questionnaires_count=models.Count("questionnaires")
        )


class Blank(BaseModel):
    title = models.CharField(max_length=255)
    slug = models.SlugField(
        max_length=255,
        default="", 
        unique=True,
        editable=False
    )
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=False)
    is_public = models.BooleanField(default=True)
    with_email_verification = models.BooleanField(default=False)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="blanks"
    )

    objects = BlankManager()
    validators = [BlankValidator()]

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.slug = slugify(self.title, Blank.objects.all())
        super().save(*args, **kwargs)

    def reindex_questions(self):
        questions = self.questions.all()
        for index, question in enumerate(questions):
            question.index = index + 1
        Question.objects.bulk_update(questions, ["index"])

    class Meta:
        db_table = "blanks"


class ParticipantManager(models.Manager):

    def get_anonymous_participant(self):
        try:
            return self.get(
                email__isnull=True, 
                first_name__exact="Anonymous",
                last_name__exact=""
            )
        except self.model.DoesNotExist:
            return self.create(
                email=None, 
                first_name="Anonymous",
                last_name=""
            )


class Participant(BaseModel):
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    email = models.EmailField(unique=True, null=True)
    user = models.OneToOneField(
        User,
        related_name="participant_profile",
        on_delete=models.CASCADE,
        blank=True, 
        null=True
    )

    objects = ParticipantManager()

    class Meta:
        db_table = "questionnaires_participants"


class Questionnaire(BaseModel):
    uuid = models.UUIDField(
        default=uuid4, 
        editable=False
    )
    blank = models.ForeignKey(
        Blank,
        on_delete=models.RESTRICT,
        related_name="questionnaires"
    )
    participant = models.ForeignKey(
        Participant,
        related_name="questionnaires",
        on_delete=models.RESTRICT
    )
    participant_device = models.CharField(max_length=255)
    participant_operation_system = models.CharField(max_length=255)
    participant_browser = models.CharField(max_length=255)
    access_code = models.CharField(
        max_length=50,
        default=partial(generate_random_string, 50),
        editable=False
    )
    created_at = models.DateTimeField(auto_now_add=True)
    started_at = models.DateTimeField(null=True, blank=True)
    finished_at = models.DateTimeField(null=True, blank=True)

    @property
    def is_in_progress(self):
        return self.started_at is not None and self.finished_at is None

    @property
    def is_finished(self):
        return self.finished_at is not None

    class Meta:
        db_table = "questionnaires"


class Question(BaseModel):
    type = models.CharField(
        max_length=50,
        choices=[(t.value, t.name) for t in QuestionType]
    )
    index = models.PositiveSmallIntegerField()
    text = models.TextField()
    post_text = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_mandatory = models.BooleanField(default=False)
    blank = models.ForeignKey(
        Blank,
        on_delete=models.CASCADE,
        related_name="questions"
    )

    class Meta:
        db_table = "questions"
        ordering = ("index",)

    def is_answered(self, questionnaire):
        return self.answers.filter(
            questionnaire__pk=questionnaire.pk
        ).exists()
        

class Answer(BaseModel):
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name="answers"
    )
    questionnaire = models.ForeignKey(
        Questionnaire,
        on_delete=models.RESTRICT,
        related_name="answers"
    )
    made_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = "answers"
        unique_together = (("question", "questionnaire"))