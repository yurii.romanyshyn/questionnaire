from contextlib import contextmanager
from django.db import connection
from django.db import models


class BaseModel(models.Model):

    class Meta:
        abstract = True

    def store(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        for validator in getattr(self, "validators", []):
            validator(self.__dict__, model=self)