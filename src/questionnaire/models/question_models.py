from django.db import models
from django.core.validators import MaxValueValidator
from django.core.exceptions import ValidationError

from .main_models import Question
from .mixins import CheckQuestionTypeMixin
from .validators import ChoicesValidator
from .validators import SelectQuestionValidator
from .validators import MultiSelectQuestionValidator
from .validators import NumberQuestionValidator
from .validators import TextQuestionValidator
from .constants import QuestionType
from .constants import TEXT_QUESTION_DEFAUT_LENGTH
from .constants import TEXT_QUESTION_MAX_LENGTH
from .constants import CHOICE_MAX_LENGTH


class TextQuestion(CheckQuestionTypeMixin, Question):
    max_length = models.PositiveIntegerField(
        default=TEXT_QUESTION_DEFAUT_LENGTH,
        validators=[MaxValueValidator(TEXT_QUESTION_MAX_LENGTH)]
    )
    default = models.TextField(
        max_length=TEXT_QUESTION_MAX_LENGTH,
        null=True, 
        blank=True
    )
    question_ptr = models.OneToOneField(
        Question,
        on_delete=models.CASCADE,
        parent_link=True,
        primary_key=True,
        related_name=QuestionType.TEXT.value
    )

    QUESTION_TYPE = QuestionType.TEXT
    validators = [TextQuestionValidator()]

    class Meta:
        db_table = "text_questions"


class SelectQuestion(CheckQuestionTypeMixin, Question):
    choices = models.JSONField(
        validators=[ChoicesValidator(CHOICE_MAX_LENGTH)]
    )
    default = models.CharField(
        max_length=CHOICE_MAX_LENGTH, 
        null=True, 
        blank=True
    )
    question_ptr = models.OneToOneField(
        Question,
        on_delete=models.CASCADE,
        parent_link=True,
        primary_key=True,
        related_name=QuestionType.SELECT.value
    )

    QUESTION_TYPE = QuestionType.SELECT
    validators = [SelectQuestionValidator()]

    class Meta:
        db_table = "select_questions"


class MultiSelectQuestion(CheckQuestionTypeMixin, Question):
    choices = models.JSONField(
        validators=[ChoicesValidator(CHOICE_MAX_LENGTH)]
    )
    default = models.JSONField(
        validators=[ChoicesValidator(CHOICE_MAX_LENGTH)],
        null=True,
        blank=True
    )
    question_ptr = models.OneToOneField(
        Question,
        on_delete=models.CASCADE,
        parent_link=True,
        primary_key=True,
        related_name=QuestionType.MULTI_SELECT.value
    )

    QUESTION_TYPE = QuestionType.MULTI_SELECT
    validators = [MultiSelectQuestionValidator()]

    class Meta:
        db_table = "multi_select_questions"


class NumberQuestion(CheckQuestionTypeMixin, Question):
    min_value = models.IntegerField()
    max_value = models.IntegerField()
    default = models.IntegerField(null=True, blank=True)
    question_ptr = models.OneToOneField(
        Question,
        on_delete=models.CASCADE,
        parent_link=True,
        primary_key=True,
        related_name=QuestionType.NUMBER.value
    )

    QUESTION_TYPE = QuestionType.NUMBER
    validators = [NumberQuestionValidator()]

    class Meta:
        db_table = "number_questions"