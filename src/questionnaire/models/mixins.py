from django.core.exceptions import ValidationError
from .main_models import Question
from .main_models import Answer
from .constants import QuestionType


class CheckQuestionTypeMixin:

    def save(self, *args, **kwargs):
        print("MIXIN")
        self.type = self.get_question_type().value
        return super().save(*args, **kwargs)

    def clean(self, *args, **kwargs):
        question_type = self.get_question_type()
        error_message = (
            f"Incorrect 'question_type' ({self.type}) "
            f"for model {self.__class__.__name__}. "
            f"Excpecting {question_type}"
        )
        
        if isinstance(self, Question):
            if question_type.value != self.type:
                raise ValidationError(error_message)
        elif isinstance(self, Answer):
            if self.question.type != question_type.value:
                raise ValidationError(error_message)
        else:
            raise TypeError("Unapropriate use of 'CheckInputTypeMixin'")

        super().clean(*args, **kwargs)

    def get_question_type(self):
        question_type = getattr(self, "QUESTION_TYPE", None)
        if question_type is None:
            raise ValueError(
                f"Error in {self.__class__.__name__}. "
                "Subclasses of Question/Answer models must specify "
                "'QUESTION_TYPE' class atribute."
            )
        if not isinstance(question_type, QuestionType):
            raise ValueError(
                "'QUESTION_TYPE' class model attribute must be of type 'QuestionType'"
            )
        return question_type