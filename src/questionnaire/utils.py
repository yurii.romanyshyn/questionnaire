import random
import string
from django.utils.text import slugify as django_slugify
from ua_parser import user_agent_parser


def slugify(value, queryset, slug_field="slug", allow_unicode=False):
    initial_slug = django_slugify(value, allow_unicode)
    generated_slug = f"{initial_slug}"
    index = 1
    while True:
        filter_args = {f"{slug_field}__exact": generated_slug}
        if not queryset.filter(**filter_args).exists():
            return generated_slug
        index += 1
        generated_slug = f"{initial_slug}-{index}"


def parse_user_agent_string(user_agent):
    parsed_ua = user_agent_parser.Parse(user_agent)
    device = parsed_ua.get('Device') or {}
    os = parsed_ua.get('os') or {}
    ua = parsed_ua.get('user_agent') or {}
    return {
        "device": (
            f"{device.get('brand') or 'Unknown'} "
            f"{device.get('family') or 'Unknown'} "
            f"{device.get('model') or 'Unknown'}"
        ).strip(),
        "os": (
            f"{os.get('family') or 'Unknown'} "
            f"{os.get('major') or 'Unknown'}"
        ).strip(),
        "browser": (
            f"{ua.get('family') or 'Unknown'} "
            f"{ua.get('major') or 'Unknown'}"
        ).strip()
    }

def generate_random_string(length):
    chars = (
        string.ascii_letters 
        + string.digits 
        + string.punctuation
    )
    return "".join(
        random.choice(chars) 
        for i in range(length)
    )