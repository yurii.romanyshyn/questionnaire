from django import template
from django import forms
from django.contrib import admin
from questionnaire.models.constants import QuestionType


register = template.Library()


@register.simple_tag
def render_question_widget(question, input_name, **kwargs):
    question_render_functions = {
        QuestionType.TEXT.value: render_text_question_widget,
        QuestionType.SELECT.value: render_select_question_widget,
        QuestionType.MULTI_SELECT.value: render_multi_select_question_widget,
        QuestionType.NUMBER.value: render_number_input_widget,
    }
    
    render_function = question_render_functions.get(question.type)
    specific_question = getattr(question, question.type)

    if render_function is None:
        return render_error_widget(question, input_name, **kwargs)

    return render_function(specific_question, input_name, **kwargs)


def render_text_question_widget(question, input_name, **kwargs):
    return forms.Textarea().render(
        name=input_name, 
        value=question.default,
        attrs={"maxlength": question.max_length},
        **kwargs
    )


def render_select_question_widget(question, input_name, **kwargs):
    return forms.Select(choices=enumerate(question.choices)).render(
        name=input_name,
        value=question.default,
        **kwargs
    )


def render_multi_select_question_widget(question, input_name, **kwargs):
    return forms.SelectMultiple(choices=question.choices).render(
        name=input_name,
        value=question.default,
        **kwargs
    )


def render_number_input_widget(question, input_name, **kwargs):
    return forms.NumberInput().render(
        name=input_name,
        value=question.default,
        **kwargs
    )


def render_error_widget(question, input_name, **kwargs):
    return (
        "<span>Error, cannnot render question widget. "
        f"Unknown question type - {question.type}</span>"
    )