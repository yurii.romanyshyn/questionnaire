from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from questionnaire.models import Blank
from questionnaire.models import Participant
from questionnaire.models.validators import BlankValidator


class BlankSerializer(serializers.ModelSerializer):
    questions_count = serializers.IntegerField(required=False)
    questionnaires_count = serializers.IntegerField(required=False)
    
    class Meta:
        model = Blank
        fields = (
            "id",
            "slug",
            "title", 
            "description", 
            "is_active", 
            "is_public",
            "with_email_verification",
            "questions_count",
            "questionnaires_count"
        )
        read_only_fields = (
            "id",
            "slug",
            "questions_count",
            "questionnaires_count"
        )
        validators = [BlankValidator()]


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "user"
        )
        read_only_fields = ("id",)
