from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from questionnaire.models import Answer
from questionnaire.models import TextAnswer
from questionnaire.models import SelectAnswer
from questionnaire.models import MultiSelectAnswer
from questionnaire.models import NumericalAnswer
from questionnaire.models.constants import QUESTIONS_ANSWERS
from questionnaire.models.constants import QuestionType
from questionnaire.models.validators import TextAnswerValidator
from questionnaire.models.validators import SelectAnswerValidator
from questionnaire.models.validators import MultiSelectAnswerValidator
from questionnaire.models.validators import NumericalAnswerValidator

from .questions_serializers import get_question_serializer


answers_general_fields = (
    "id",
    "made_at",
    "updated_at"
)

general_read_only_fields = (
    "id",
    "question",
    "questionnaire",
    "made_at",
    "udated_at"
)


class TextAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextAnswer
        fields = answers_general_fields + ("value", )
        read_only_fields = general_read_only_fields
        validators = [
            TextAnswerValidator()
        ]


class SelectAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelectAnswer
        fields = answers_general_fields + ("value", )
        read_only_fields = general_read_only_fields
        validators = [
            SelectAnswerValidator()
        ]


class MultiSelectAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultiSelectAnswer
        fields = answers_general_fields + ("value", )
        read_only_fields = general_read_only_fields
        validators = [
            MultiSelectAnswerValidator()
        ]


class NumericalAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = NumericalAnswer
        fields = answers_general_fields + ("value", )
        read_only_fields = general_read_only_fields
        validators = [
            NumericalAnswerValidator()
        ]


def get_answer_serializer(question_type):
    serializers = {
        QuestionType.TEXT.value: TextAnswerSerializer,
        QuestionType.SELECT.value: SelectAnswerSerializer,
        QuestionType.MULTI_SELECT.value: MultiSelectAnswerSerializer,
        QuestionType.NUMBER.value: NumericalAnswerSerializer
    }
    try:
        return serializers[question_type]
    except KeyError:
        raise ValueError(f"Unknown question type {question_type}")
    

    
class AnswerReprSerializer(serializers.BaseSerializer):

    def __init__(self, *args, **kwargs):
        self.resolve_questions = kwargs.pop("resolve_questions", False)
        super().__init__(*args, **kwargs)
    
    def to_internal_value(self, value):
        raise NotImplementedError

    def to_representation(self, instance, *args, **kwargs):
        if self.resolve_questions:
            question = getattr(instance.question, instance.question.type)
            answer = getattr(instance, QUESTIONS_ANSWERS[question.type])
            AnswerSerializer = get_answer_serializer(question.type)
            QuestionSerializer = get_question_serializer(question.type)
            data = AnswerSerializer(instance=answer).data
            data["question"] = QuestionSerializer(instance=question).data
            return data
        else:
            AnswerSerializer = get_answer_serializer(instance.question.type)
            return AnswerSerializer(instance=instance).data