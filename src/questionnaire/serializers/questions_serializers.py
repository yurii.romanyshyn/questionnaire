from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from questionnaire.models import Question
from questionnaire.models import TextQuestion
from questionnaire.models import SelectQuestion
from questionnaire.models import MultiSelectQuestion
from questionnaire.models import NumberQuestion
from questionnaire.models.validators import SelectQuestionValidator
from questionnaire.models.validators import MultiSelectQuestionValidator
from questionnaire.models.validators import NumberQuestionValidator
from questionnaire.models.validators import TextQuestionValidator
from questionnaire.models.constants import QuestionType


question_general_fields = (
    "id",
    "index",
    "type",
    "text",
    "post_text",
    "is_active",
    "is_mandatory",
)


question_readonly_fields = ("id", "index")


class GeneralMixin:

    def validate_type(self, value):
        if self.instance:
            if self.instance.type != value:
                raise ValidationError("You cannot change question type")
        return value


class TextQuestionSerializer(GeneralMixin, serializers.ModelSerializer):
    class Meta:
        model = TextQuestion
        fields = question_general_fields + ("max_length", "default")
        read_only_fields = question_readonly_fields
        validators = [TextQuestionValidator()]


class SelectQuestionSerializer(GeneralMixin, serializers.ModelSerializer):
    class Meta:
        model = SelectQuestion
        fields = question_general_fields + ("choices", "default")
        read_only_fields = question_readonly_fields
        validators = [SelectQuestionValidator()]


class MultiSelectQuestionSerializer(GeneralMixin, serializers.ModelSerializer):
    class Meta:
        model = MultiSelectQuestion
        fields = question_general_fields + ("choices", "default")
        read_only_fields = question_readonly_fields
        validators = [MultiSelectQuestionValidator()]


class NumberQuestionSerializer(GeneralMixin, serializers.ModelSerializer):
    class Meta:
        model = NumberQuestion
        fields = question_general_fields + ("min_value", "max_value", "default")
        read_only_fields = question_readonly_fields
        validators = [NumberQuestionValidator()]


def get_question_serializer(question_type):
    serializers = {
        QuestionType.TEXT.value: TextQuestionSerializer,
        QuestionType.SELECT.value: SelectQuestionSerializer,
        QuestionType.MULTI_SELECT.value: MultiSelectQuestionSerializer,
        QuestionType.NUMBER.value: NumberQuestionSerializer
    }
    try:
        return serializers[question_type]
    except KeyError:
        raise ValueError(f"Unknown question type {question_type}")


class QuestionReprSerializer(serializers.BaseSerializer):

    def to_internal_value(self, data):
        raise NotImplementedError

    def to_representation(self, instance, *args, **kwargs):
        question_serializer = get_question_serializer(instance.type)
        return question_serializer(instance=getattr(instance, instance.type)).data