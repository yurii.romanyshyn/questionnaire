from django.urls import path
from .blank_views import ListBlanksView
from .blank_views import CreateBlankView
from .blank_views import UpdateBlankView
from .blank_views import DestroyBlankView
from .blank_views import BulkActionBlankView
from .question_views import CreateQuestionView
from .question_views import UpdateQuestionView
from .question_views import DestroyQuestionView
from .question_views import ListQuestionsView
from .question_views import QuestionBulkActionView
from .questionnaire_views import ReprQuestionnaireView
from .questionnaire_views import BecomeParticipantView
from .questionnaire_views import BecomeAnonymousParticipantView
from .questionnaire_views import RunQuestionnaireView

urlpatterns = [
    
    path(
        "blanks/", 
        ListBlanksView.as_view(), 
        name="list-blanks"
    ),
    
    path(
        "blank/create/", 
        CreateBlankView.as_view(), 
        name="create-blank"
    ),
    
    path(
        "blank/<slug:blank_slug>/update/", 
        UpdateBlankView.as_view(), 
        name="update-blank"
    ),
    
    path(
        "blank/<slug:blank_slug>/delete/", 
        DestroyBlankView.as_view(),
        name="destroy-blank"
    ),
    
    path(
        "blanks/bulk-action/", 
        BulkActionBlankView.as_view(), 
        name="blank-bulk-action"
    ),
    
    path(
        "blank/<slug:blank_slug>/questions/",
        ListQuestionsView.as_view(),
        name="list-questions"
    ),
    
    path(
        "blank/<slug:blank_slug>/question/create/", 
        CreateQuestionView.as_view(), 
        name="question-create"
    ),
    
    path(
        "blank/<slug:blank_slug>/question/<int:question_pk>/update/",
        UpdateQuestionView.as_view(),
        name="update-question"
    ),
    
    path(
        "blank/<slug:blank_slug>/question/<int:question_pk>/delete/",
        DestroyQuestionView.as_view(),
        name="destroy-question"
    ),

    path(
        "blank/<slug:blank_slug>/questions/bulk-action/",
        QuestionBulkActionView.as_view(),
        name="question-bulk-action"
    ),

    path(
        "questionnaires/blank/<slug:blank_slug>/",
        ReprQuestionnaireView.as_view(),
        name="repr-questionnaire"
    ),

    path(
        "questionnaires/blank/<slug:blank_slug>/become-participant/",
        BecomeParticipantView.as_view(),
        name="become-participant"
    ),

    path(
        "questionnaires/blank/<slug:blank_slug>/become-anonymous-participant/",
        BecomeAnonymousParticipantView.as_view(),
        name="become-anonymous-participant"
    ),

    path(
        "questionnaire/<uuid:uuid>/",
        RunQuestionnaireView.as_view(),
        name="run-questionnaire"
    )

]