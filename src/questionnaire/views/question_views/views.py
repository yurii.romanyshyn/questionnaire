from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404
from django.shortcuts import render
from django.shortcuts import redirect
from django.db.models import F
from django.db.transaction import atomic
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings

from questionnaire.forms import get_question_form
from questionnaire.models import Question
from questionnaire.models.constants import QuestionType
from questionnaire.views.blank_views.mixins import BlankLookupMixin

from .constants import QuestionBulkAction
from .forms import QuestionTypeForm
from .mixins import QuestionLookupMixin


class ListQuestionsView(LoginRequiredMixin, BlankLookupMixin, ListView):
    template_name = "questionnaire/questions-list.html"
    context_object_name = "questions_list"
    paginate_by = settings.QUESTIONS_PER_PAGE
    extra_context = {
        "actions": [(a.name, a.value) for a in QuestionBulkAction],
        "questions_types": [(type.name, type.value) for type in QuestionType]
    }

    def get_queryset(self):
        return (
            self.get_blank().questions
            .order_by("index")
            .all()
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["blank"] = self.get_blank()
        return context


class QuestionBulkActionView(LoginRequiredMixin, BlankLookupMixin, View):
    action_payload_kwarg = "name"
    questions_payload_kwarg = "questions"
    questions_lookup = "pk__in"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions = {}
        for action in QuestionBulkAction:
            self.actions[action.name] = getattr(self, f"{action.name}_action")

    def post(self, request, *args, **kwargs):
        action = request.POST[self.action_payload_kwarg]
        action_method = self.actions[action]
        action_method()
        return redirect(reverse(
            "list-questions",
            kwargs={"blank_slug": self.get_blank().slug}
        ))

    def destroy_action(self):
        blank = self.get_blank()
        with atomic():
            self.get_questions_queryset().delete()
            blank.reindex_questions()


    def toggle_is_active_action(self):
        questions = self.get_questions()
        for question in questions:
            question.is_active = not question.is_active
        Question.objects.bulk_update(questions, ["is_active"])
        
    def toggle_is_mandatory_action(self):
        questions = self.get_questions()
        for question in questions:
            question.is_mandatory = not question.is_mandatory
        Question.objects.bulk_update(questions, ["is_mandatory"])

    def get_questions_lookup(self):
        lookup_value = self.request.POST.getlist(self.questions_payload_kwarg)
        lookup = {self.questions_lookup: lookup_value}
        return lookup

    def swap_questions_action(self):
        questions = self.get_questions()
        if len(questions) == 2:
            questions[0].index, questions[1].index = questions[1].index, questions[0].index
            questions[0].save()
            questions[1].save()

    def get_questions_queryset(self):
        blank = self.get_blank()
        return (
            blank.questions
            .filter(**self.get_questions_lookup())
            .order_by("index")
            .all()
        )

    def get_questions(self):
        blank = self.get_blank()
        return get_list_or_404(
            blank.questions.order_by("index"),
            **self.get_questions_lookup()
        )


class CreateQuestionView(LoginRequiredMixin, BlankLookupMixin, View):
    template_name = "questionnaire/create-question-form.html"

    def get(self, request, *args, **kwargs):
        blank = self.get_blank()
        question_type_form = QuestionTypeForm(request.GET)
        
        if not question_type_form.is_valid():
            return HttpResponseBadRequest("Unknown question type")
        
        question_type_value = question_type_form.cleaned_data["type"]
        QuestionForm = get_question_form(question_type_value)
        
        return render(request, self.template_name, context={
            "form": QuestionForm(),
            "blank": blank,
        })

    def post(self, request, *args, **kwargs):
        blank = self.get_blank()
        question_type_form = QuestionTypeForm(request.POST)

        if not question_type_form.is_valid():
            return HttpResponseBadRequest("Unknown question type")

        question_type_value = question_type_form.cleaned_data["type"]
        QuestionForm = get_question_form(question_type_value)
        form = QuestionForm(request.POST)
        
        if form.is_valid():
            question = form.save(commit=False)
            question.blank = blank
            question.index = blank.questions.count() + 1
            question.save()
            return redirect(reverse(
                "list-questions",
                kwargs={"blank_slug": blank.slug}
            ))
        else:
            return render(request, self.template_name, context={
                "form": form,
                "blank": blank,
            })            


class UpdateQuestionView(
    LoginRequiredMixin, 
    BlankLookupMixin, 
    QuestionLookupMixin, 
    View
):
    template_name = "questionnaire/update-question-form.html"
    questions_types_values = [qt.value for qt in QuestionType]
    
    def get(self, request, *args, **kwargs):
        question = self.get_question(prefetch_related=self.questions_types_values)
        QuestionForm = get_question_form(question.type)
        specific_question = getattr(question, question.type)
        return render(request, self.template_name, context={
            "form": QuestionForm(instance=specific_question),
            "question": specific_question,
            "blank": self.get_blank()
        })

    def post(self, request, *args, **kwargs):
        question = self.get_question(prefetch_related=self.questions_types_values)
        QuestionForm = get_question_form(question.type)
        specific_question = getattr(question, question.type)
        form = QuestionForm(request.POST, instance=specific_question)
        if form.is_valid():
            form.save()
            return redirect(reverse(
                "list-questions", 
                kwargs={"blank_slug": self.get_blank().slug}
            ))
        else:
            return render(request, self.template_name, context={
                "form": QuestionForm(instance=specific_question),
                "blank": self.get_blank(),
                "question": specific_question
            })


class DestroyQuestionView(
    LoginRequiredMixin,
    BlankLookupMixin,
    QuestionLookupMixin,
    View
):
    
    def post(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        blank = self.get_blank()
        question = self.get_question()
        
        with atomic():
            question.delete()
            (
                Question.objects
                .filter(blank=blank)
                .filter(index__gt=question.index)
                .update(index=F("index") - 1)
            )
        
        return redirect(reverse(
            "list-questions",
            kwargs={"blank_slug": self.get_blank().slug}
        ))