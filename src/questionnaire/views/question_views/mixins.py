from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404


class QuestionLookupMixin:
    question_url_kwarg = "question_pk"
    question_lookup_field = "pk"

    def get_question_lookup(self):
        lookup_value = self.kwargs[self.question_url_kwarg]
        lookup = {self.question_lookup_field: lookup_value}
        return lookup

    def get_question_queryset(self, prefetch_related=None):
        blank = self.get_blank()
        return (
            blank.questions.all() 
            if prefetch_related is None 
            else blank.questions.prefetch_related(*prefetch_related).all()
        )

    def filter_question_queryset(self):
        return (
            self.get_question_queryset()
            .filter(**self.get_question_lookup())
            .all()
        )

    def get_question(self, prefetch_related=None):
        if getattr(self, "question", None) is None:
            self.question = get_object_or_404(
                self.get_question_queryset(prefetch_related),
                **self.get_question_lookup()
            )
        return self.question

    def get_questions(self, prefetch_related=None):
        return get_list_or_404(
            self.get_question_queryset(prefetch_related),
            **self.get_question_lookup()
        )