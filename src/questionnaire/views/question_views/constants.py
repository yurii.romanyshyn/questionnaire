from enum import Enum


class QuestionBulkAction(str, Enum):
    destroy = "Destroy action"
    toggle_is_active = "Toggle is active"
    toggle_is_mandatory = "Toggle is mandatory"
    swap_questions = "Swap questions"

    @classmethod
    def from_value(cls, value):
        for entry in cls:
            if entry.value == value:
                return entry
        raise ValueError("Unknown value")