from django import forms
from questionnaire.models.constants import QuestionType


class QuestionTypeForm(forms.Form):
    type = forms.ChoiceField(
        choices=[(type.value, type.name) for type in QuestionType],
        required=True
    )
