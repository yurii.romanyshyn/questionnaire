

class UserCanBecomeParticipantMixin:
    def user_can_become_participant(self, blank):
        return (
            blank.is_public
            or self.request.user.is_authenticated
        )


class UserCanRunQuestionnaireMixin:
    def user_can_run_questionnaire(self, questionnaire):
        request = self.request
        return (
            (
                request.user.is_authenticated
                and questionnaire.participant.user.pk == request.user.pk
            )
            or request.session.get(str(questionnaire.uuid)) == questionnaire.access_code
        )
