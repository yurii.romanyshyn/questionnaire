from datetime import datetime
from django.core.mail import send_mail
from django.db.models import Count
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.http import HttpResponseForbidden

from questionnaire.models import Blank
from questionnaire.models import Question
from questionnaire.models import Participant
from questionnaire.models import Questionnaire
from questionnaire.models.constants import QuestionType
from questionnaire.utils import parse_user_agent_string
from questionnaire.views.blank_views.mixins import BlankLookupMixin

from .mixins import UserCanBecomeParticipantMixin
from .mixins import UserCanRunQuestionnaireMixin
from .forms import BecomeParticipantForm


class ReprQuestionnaireView(
    UserCanBecomeParticipantMixin, 
    BlankLookupMixin,
    View
):
    template_name = "questionnaire/repr-questionnaire.html"
    blank_url_kwarg = "blank_slug"
    blank_lookup_field = "slug__exact"

    def get_blank_queryset(self):
        return (
            Blank.objects
            .annotate(questions_count=Count("questions"))
            .filter(is_active=True)
            .filter(questions_count__gt=0)
        )

    def get(self, request, *args, **kwargs):
        blank = self.get_blank()
        if not self.user_can_become_participant(blank):
            return HttpResponseForbidden("You cannot access this page")
        return render(request, self.template_name, context={
            "blank": blank
        })


def get_user_agent(request):
    user_agent_string = request.META["HTTP_USER_AGENT"]
    user_agent = parse_user_agent_string(user_agent_string)
    return user_agent


class BecomeParticipantView(
    LoginRequiredMixin, 
    BlankLookupMixin, 
    View
):
    blank_url_kwarg = "blank_slug"
    blank_lookup_field = "slug__exact"
        
    def post(self, request, *args, **kwargs):
        blank = self.get_blank()
        
        participant_profile = self.get_participant_profile()
        user_agent = get_user_agent(request)
        
        questionnaire = Questionnaire.objects.create(
            blank=blank,
            participant=participant_profile,
            participant_browser=user_agent["browser"],
            participant_device=user_agent["device"],
            participant_operation_system=user_agent["os"]
        )

        request.session[str(questionnaire.uuid)] = questionnaire.access_code
        return redirect(reverse("run-questionnaire", kwargs={"uuid": questionnaire.uuid}))

    def get_blank_queryset(self):
        return (
            Blank.objects
            .annotate(questions_count=Count("questions"))
            .filter(is_active=True)
            .filter(questions_count__gt=0)
        ) 

    def get_participant_profile(self):
        try:
            user = self.request.user
            participant_profile = Participant.objects.get(email__exact=user.email)
            if participant_profile.user is None:
                participant_profile.user = user
                participant_profile.save()
            return participant_profile
        except Participant.DoesNotExist:
            return Participant.objects.create(
                first_name=user.first_name,
                last_name=user.last_name,
                email=user.email,
                user=user
            )


class BecomeAnonymousParticipantView(
    UserCanBecomeParticipantMixin,
    BlankLookupMixin,
    View
):
    blank_url_kwarg = "blank_slug"
    blank_lookup_field = "slug__exact"

    def post(self, request, *args, **kwargs):
        blank = self.get_blank()
        anonymous_participant = Participant.objects.get_anonymous_participant()
        user_agent = get_user_agent(request)

        questionnaire = Questionnaire.objects.create(
            blank=blank,
            participant=anonymous_participant,
            participant_browser=user_agent["browser"],
            participant_device=user_agent["device"],
            participant_operation_system=user_agent["os"]
        )

        request.session[str(questionnaire.uuid)] = questionnaire.access_code

        return redirect(reverse(
            "run-questionnaire",
            kwargs={"uuid": questionnaire.uuid}
        ))

    def get_blank_queryset(self):
        return (
            Blank.objects
            .annotate(questions_count=Count("questions"))
            .filter(is_active=True)
            .filter(is_public=True)
            .filter(questions_count__gt=0)            
        )

class RunQuestionnaireView(UserCanRunQuestionnaireMixin, View):
    template_name = "questionnaire/questionnaire-form.html"
    questionnaire_lookup_url_kwarg = "uuid"
    questionnaire_lookup_field = "uuid__exact"

    def get(self, request, *args, **kwargs):
        questionnaire = self.get_questionnaire()

        if not self.user_can_run_questionnaire(questionnaire):
            return HttpResponseForbidden(
                "You are not allowed to access this page"
            )

        if questionnaire.started_at is None:
            questionnaire.started_at = datetime.now()
            questionnaire.save()

        return render(request, self.template_name, context={
            "questionnaire": questionnaire,
            "blank": questionnaire.blank,
            "participant": questionnaire.participant,
            "questions": questionnaire.blank.questions.all()
        })

    def post(self, request, *args, **kwargs):
        pass

    def get_questionnaire(self):
        lookup_value = self.kwargs[self.questionnaire_lookup_url_kwarg]
        lookup = {self.questionnaire_lookup_field: lookup_value}
        queryset = (
            Questionnaire.objects
            .filter(finished_at__isnull=True)
            .select_related("blank", "participant")
            .prefetch_related(Prefetch(
                "blank__questions",
                queryset=(
                    Question.objects
                    .prefetch_related(*[qt.value for qt in QuestionType])
                    .order_by("index")
                )
            ))
        )
        return get_object_or_404(queryset, **lookup)