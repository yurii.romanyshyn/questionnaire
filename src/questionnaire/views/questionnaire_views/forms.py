from django import forms
from questionnaire.models import Participant    


class BecomeParticipantForm(forms.Form):
    first_name = forms.CharField(max_length=255)
    last_name = forms.CharField(max_length=255)
    email = forms.EmailField()