from django.shortcuts import get_list_or_404
from django.shortcuts import render
from django.shortcuts import redirect
from django.db.models import Count
from django.db.transaction import atomic
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings

from questionnaire.models import Blank
from questionnaire.forms import CreateBlankForm
from questionnaire.forms import get_question_form

from .mixins import BlankLookupMixin
from .constants import BlankBulkActions


class ListBlanksView(LoginRequiredMixin, ListView):
    template_name = "questionnaire/blank-list.html"
    context_object_name = "blanks_list"
    paginate_by = settings.BLANKS_PER_PAGE
    extra_context = {"actions": [(a.name, a.value) for a in BlankBulkActions]}

    def get_queryset(self):
        return (
            self.request.user.blanks
            .annotate(questions_count=Count("questions"))
            .all()
        )


class BulkActionBlankView(LoginRequiredMixin, BlankLookupMixin, View):
    success_url = reverse_lazy("list-blanks")
    blanks_lookup = "slug__in"
    blanks_payload_kwarg = "blanks"
    action_payload_kwarg = "name"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions = {}
        for action in BlankBulkActions:
            self.actions[action.name] = getattr(self, f"{action.name}_action")

    def post(self, request, *args, **kwargs):
        action_name = request.POST[self.action_payload_kwarg]
        action_method = self.actions[action_name]
        action_method()
        return redirect(self.success_url)

    def destroy_action(self):
        self.get_blanks_queryset().delete()

    def toggle_is_public_action(self):
        blanks = self.get_blanks()
        for blank in blanks:
            blank.is_public = not blank.is_public
        Blank.objects.bulk_update(blanks, fields=["is_public"])

    def toggle_is_active_action(self):
        blanks = self.get_blanks()
        with atomic():
            for blank in blanks:
                blank.is_active = not blank.is_active
                blank.full_clean()
                blank.save()

    def toggle_with_email_verification_action(self):
        blanks = self.get_blanks()
        for blank in blanks:
            blank.with_email_verification = not blank.with_email_verification
        Blank.objects.bulk_update(blanks, fields=["with_email_verification"])
    
    def get_blanks_lookup(self):
        lookup_value = self.request.POST.getlist(self.blanks_payload_kwarg)
        lookup = {self.blanks_lookup: lookup_value}
        return lookup

    def get_blanks_queryset(self):
        return (
            self.request.user.blanks
            .filter(**self.get_blanks_lookup())
            .all()
        )

    def get_blanks(self):
        return get_list_or_404(
            self.request.user.blanks.all(),
            **self.get_blanks_lookup()
        )


class CreateBlankView(LoginRequiredMixin, View):
    template_name = "questionnaire/create-blank-form.html"
    form_class = CreateBlankForm
    success_url = reverse_lazy("list-blanks")

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, context={
            "form": form, 
            "action": reverse("create-blank")
        })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            blank = form.save(commit=False)
            blank.owner = request.user
            blank.save()
            return redirect(self.success_url)
        else:
            return render(request, self.template_name, context={
                "form": form,
                "action": reverse("create-blank")
            })


class UpdateBlankView(LoginRequiredMixin, BlankLookupMixin, View):
    template_name = "questionnaire/update-blank-form.html"
    form_class = CreateBlankForm
    success_url = reverse_lazy("list-blanks")

    def get(self, request, *args, **kwargs):
        blank = self.get_blank()
        form = self.form_class(instance=blank)
        return render(request, self.template_name, context={
            "form": form,
            "blank": blank
        })

    def post(self, request, *args, **kwargs):
        blank = self.get_blank()
        form = self.form_class(request.POST, instance=blank)
        if form.is_valid():
            form.save()
            return redirect(self.success_url)
        else:
            return render(request, self.template_name, context={
                "form": form,
                "blank": blank
            })


class DestroyBlankView(LoginRequiredMixin, BlankLookupMixin, View):
    success_url = reverse_lazy("list-blanks")

    def post(self, rquest, *args, **kwargs):
        return self.delete(rquest, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        blank = self.get_blank()
        blank.delete()
        return redirect(self.success_url)