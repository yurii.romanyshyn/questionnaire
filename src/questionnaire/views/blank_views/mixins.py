from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404


class BlankLookupMixin:
    blank_url_kwarg = "blank_slug"
    blank_lookup_field = "slug__exact"

    def get_blank_lookup(self):
        lookup_value = self.kwargs[self.blank_url_kwarg]
        lookup = {self.blank_lookup_field: lookup_value}
        return lookup

    def get_blank_queryset(self):
        return self.request.user.blanks.all()

    def filter_blank_queryset(self):
        return (
            self.get_blank_queryset()
            .filter(**self.get_blank_lookup())
            .all()
        )

    def get_blank(self):
        if getattr(self, "blank", None) is None:
            self.blank = get_object_or_404(
                self.get_blank_queryset(),
                **self.get_blank_lookup()
            )
        return self.blank

    def get_blanks(self):
        return get_list_or_404(
            self.get_blank_queryset(),
            self.get_blank_lookup()
        )