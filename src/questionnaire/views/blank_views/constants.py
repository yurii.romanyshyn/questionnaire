from enum import Enum


class BlankBulkActions(str, Enum):
    destroy = "Delete blank"
    toggle_is_public = "Toggle 'is public' value"
    toggle_is_active = "Toggle 'is active' value"
    toggle_with_email_verification = "Toggle 'with email verification' value"

    @classmethod
    def from_value(cls, value):
        for entry in cls:
            if entry.value == value:
                return entry
        raise ValueError("Unknown value")
