from .views import ListBlanksView
from .views import CreateBlankView
from .views import UpdateBlankView
from .views import DestroyBlankView
from .views import BulkActionBlankView