from django.contrib import admin
from django.urls import path
from django.urls import include
from rest_framework.authtoken import views


urlpatterns = [
    path("admin/", admin.site.urls),
    #
    path("api-auth/", views.obtain_auth_token),
    path("api/", include("questionnaire.api.urls")),
    path("api/", include("users.api.urls")),
    #
    path("", include("users.views.urls")),
    path("", include("questionnaire.views.urls")),
]
